# Set the 'Apartado1' of the repository as the working directory

# Quality control for the raw sequence fastq files

mkdir -p ./results/fastQC

for sample in $(ls input/*.chr21.fastq | cut -d '.' -f1 | sed 's:input/::')
do
	if [ -f "results/fastQC/$sample.chr21_fastqc.zip" ]
	then
		echo "Quality analysis for sample $sample already done, skipping..."
	else
		fastqc -o ./results/fastQC --noextract input/${sample}.chr21.fastq
	fi
done

# Reference chromosome indexing

if [ -f "input/REF/chr21.1.ht2" ]
then
	echo "Reference chromosome already aligned, skipping..."
else
	hisat2-build --seed 123 -p 2 input/REF/chr21.fa input/REF/chr21
fi

# Read alignment and post-alignment processing

mkdir -p results/alignment

for sample in $(ls input/*.chr21.fastq | cut -d '.' -f1 | sed 's:input/::')
do
	if [ -f "results/alignment/$sample.sorted.bam" ]
	then
		echo "Reads of sample $sample already aligned, skipping..."
	else
		
		# Read alignment
		hisat2 --new-summary --summary-file results/alignment/${sample}.hisat2.summary --rna-strandness R --seed 123 --phred33 -p 2 -k 1 -x input/REF/chr21 -U input/${sample}.chr21.fastq -S results/alignment/${sample}.sam

		# sam to bam conversion, sorting and indexing
		samtools view -bh -S results/alignment/${sample}.sam > results/alignment/${sample}.bam
		samtools sort results/alignment/${sample}.bam -o results/alignment/${sample}.sorted.bam
		samtools index results/alignment/${sample}.sorted.bam

	fi
done

# Read count

mkdir -p results/read_counts

for sample in $(ls results/alignment/*.sorted.bam | cut -d '.' -f1 | sed 's:results/alignment/::')
do
	if [ -f "results/read_counts/$sample.htseq" ]
	then
		echo "Reads of sample $sample already counted, skipping..."
	else
		htseq-count --format=bam --stranded=reverse --mode=intersection-nonempty --minaqual=10 --type=exon --idattr=gene_id --additional-attr=gene_name results/alignment/${sample}.sorted.bam input/REF/GRCh38.gencode.v38.annotation.for.chr21.gtf > results/read_counts/${sample}.htseq

	fi
done

# Create the raw and normalized counts files (.tsv)

if [ -f "results/read_counts/counts_normalized.tsv" ]
then
	echo "Raw and normalized count files already created, skipping..."
else
	Rscript scripts/raw_norm_counts.R
fi


