To replicate these analyses, first create the conda environment from the yaml file.
Then, to run the first pipeline, go to the "Apartado1" subdirectory and run the bash-coded "scripts/pipeline.sh" file.
For the second analysis, run the R scripts in the "scripts" directory in "Apartado2".
